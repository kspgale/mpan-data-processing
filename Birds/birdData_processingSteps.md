# Bird effort-corrected density data for NSB MPA Network
Written 6 July 2020

## Input data pre-processing (CWS and/or Carrie did this)
Data from two sets of surveys (Raincoast and non-Raincoast) were combined. The collated surveys employed two different survey methods (strip transects and distance sampling) and were therefore compiled separately, as follows.

### Raincoast Data
- Shapefile data was downloaded from http://seamap.env.duke.edu/dataset/1458 (alternate link: https://scholarworks.alaska.edu/handle/11122/7347)
- Estimated density values were calculated by Raincoast using distance sampling, and were provided in the data download as shapefiles.
- The transect segments pertaining to the appropriate species and season were extracted and spatially joined to the 1 km x 1 km MPA network planning grid. The density and total surveys per grid cell were summed and used to calculate the average density of birds observed in each grid cell.

### Non-Raincoast surveys
- These surveys used strip transects. 
- Data were obtained from [CWS?]
- The following data sources were included in this analysis:
	- Bird Studies Canada - Coastal Waterbird Survey
	- Environment and Climate Change Canada (Canadian Wildlife Service)
		- Coastal Waterbird Inventory
		- Marine Bird Database
		- Moulting Seaduck Survey
		- Pelagic Seabird Surveys
	- Laskeek Bay Conservation Society - Surveys
	- US Geological Survey - North Pacific Seabird Database
- Survey polygons were created based on the transect length and survey width. For each survey polygon, the total number of birds observed from the species in the given species group and for the chosen season were summed and used to calculate the density of birds per sq. km. In addition, the total number of surveys conducted for that polygon and the surveys where the given species were observed were determined. The survey polygons from all datasets were merged and spatially joined to the 1 km x 1 km planning grid and the density and survey totals were summed and used to calculate the average density of birds observed in each grid cell.

## Combining the 2 datasources and processing for MPA Network 
---------------
STEPS: 
1. Normalize
2. Calculate quantiles
3. Transform
4. Drop lowest quantile

### Rescaling/"Normalizing"(Carrie did this)
- The two bird density datasets were normalized by dividing each value by the maximum value in each dataset. The resulting density index ranged from 0 to 1. 
- The normalized density estimates from the two datasets were overlaid onto the MPA Network 1 km planning grid and averaged together within each grid cell by species. 
- Output geodatabase: `mpatt_eco_birds_density_seasketch_20180127.gdb` 
	- Many fields in the attribute table have been carried through from the analysis, but the most important are:
			- 'PU_ID': Planning Unit ID
			- `reclassFinal`: The normalized density, averaged for all surveys in that planning unit. Renamed 'densNorm' in subsequent steps. 

### Transforming & Reprocessing for data archiving 
#### Carrie originally did this in June 2018, Katie reprocessed the data in July 2020.
Scripts: `transformBirdData.py`, `compare_PythonOutput_vs_20190325.R`
Input data: `mpatt_eco_birds_density_seasketch_20180127.gdb`; `mpatt_eco_20190325.gdb` (for AbsMinLog values)
Output gdb: `birds_reprocessed_2020-07-06.gdb`

- The 2019-03-25 analysis version had had its zeros removed and did contained unneeded attributes (avg_densit and poly_recla) and was missing fields for the intermediate transformation steps.
- The zeros represent areas that were surveyed, but in which the species was not observed. These zeros are not important for Marxan or CGA analysis, but are relevant for data visualization to be clear about the data's scope. 
- For an archive version, I wanted:
	- Fields for the rescaled (pre-transformed) density values, the transformed values, and a field called `value`, which is a duplicate of the transformed value field. 
	- Zeros representing areas sampled
- The rescaled bird density values ranged from 0-1, but were heavily skewed towards 0. This was constraining the prioritized planning units in Marxan, since there was such a difference between the highest and lowest values. 
- The species experts at CWS recommended that the lowest quantile be excluded from site selection analysis and subsequent analytics. 
- To normalize the distribution of values the rescaled density values were transformed: ln(n)+abs(min(ln(n)))+1. (i.e., for each density value, log the value, add the absolute value of the minimum logged density value for that species, plus 1). For birds, abs(min(ln(n))) (*AbsMinLog*) was calculated only on the top 4 quantiles.
- Processing note: 
	- In June 2018, Carrie manually calculated/used ModelBuilder to calculate the quantiles, drop the lowest quantile, and apply the transformation. In July 2020 I used python/numpy to do the same using the same input data (2019-01-27), but *could not replicate the final values exactly*. The results for some features are identical (e.g., albatross, gooseswan) but some are off by a small amount (e.g., cormorant PU 194 has a final value of 2.66863 in my version and 2.68564 in Carrie's, resulting from an AbsMinLog of 7.28021 vs 7.29722). In looking at the data, the input values (reclassFinal and the log values) match, but there are discrepencies around the quantile cutoffs. This is likely because of differences in how the quantiles were calculated (i.e., were the categories assigned based on >= or < ?) and/or differences in floats/number of decimal places.
	- I wanted the values in the archive version of this data to match what was used in the CGA/Marxan (i.e., 2019-03-25). As such, I calculated AbsMinLog for each species from 2019-03-25, and used that value to transform 2018-01-27. 
	- Using script `compare_PythonOutput_vs_20190325.R`, I confirmed that the values from 2019-03-25 and the outputs of `transformBirdData.py` (using 2018-01-27 as input) match, besides some rounding differences in the 6th+ decimal places.

### Converting shape
Script: `clipRenameProject.py`
Input data: `birds_reprocessed_2020-07-06.gdb`; `MaPP_Marine_Area_EstuaryCorrected.shp` (planning area)
Output gdb: `birds_clip_project_2020-07-06.gdb`
- The features with the correct values were to be clipped to the planning area, projected to BC Albers, and renamed to match the data in mpatt_eco_20190325.gdb.



#-------------------------------------------------------------------------------
# Title:        Process bird density data for MPA Network analysis
#
# Authors:      Katie Gale
# Affiliation:  Fisheries and Oceans Canada (DFO)
# Group:        Marine Spatial Ecology and Analysis
# Location:     Institute of Ocean Sciences
# Contact:      katie.gale@dfo-mpo.gc.ca
# Project:      NSB MPA Network
#
# Version date: 8 July 2020
#
# Overview:
# - Start with original bird density data (version date 20180127):
#   - 113839 planning units (full planning area), including null values (not-surveyed) and 0s (surveyed, species not seen)
#   - "reclassFinal" is the average of rescaled (normalized) values from a) Raincoast and b) other bird surveys, and is considered "normalized density".
# - Move original bird density data into new geodatabase, calculate quantiles, transform the values, and clean up and attach attributes to featureClass
#
# Requirements:
# * mpatt_eco_birds_density_seasketch_20180127.gdb
# * mpatt_eco_20190325.gdb
#-------------------------------------------------------------------------------


import os
import arcpy
from arcpy.sa import *
import math
import numpy as np
import regex
import pandas as pd

# Set environment settings
ingdb = r"D:\Documents\Data files\MPATT Data\DataLayers\forMarxanAndSeasketch\Archive\mpatt_eco_birds_density_seasketch_20180127.gdb"
arcpy.env.workspace = ingdb
inFeat = arcpy.ListFeatureClasses()
inFeat.sort()

outgdb = r"D:\Documents\Data files\SpeciesData\Birds\2020-06-25_ReprocessBirdData\birds_reprocessed_2020-07-06.gdb"
outcsv = r'D:\Documents\Data files\SpeciesData\Birds\2020-06-25_ReprocessBirdData\2020-07-06_tables\tab'

#combine the names of the untransformed inputs with the transformed data Carrie originally did
dataPrevTransf = r"D:\Documents\Data files\MPATT Data\DataLayers\Eco gdbs\mpatt_eco_20190325.gdb"
arcpy.env.workspace = dataPrevTransf
featPrevTransf = arcpy.ListFeatureClasses("*_birds_*_density*")
featPrevTransf.sort()

matchFeat = {u'mpatt_eco_birds_albatross_density_data': u'eco_birds_albatross_density_m',
                u'mpatt_eco_birds_cormorant_density_data': u'eco_birds_cormorant_density_m',
                u'mpatt_eco_birds_cormorant_density_winter_data': u'eco_birds_cormorant_densitywinter_m',
                u'mpatt_eco_birds_goose_swan_density_data': u'eco_birds_gooseswan_density_m',
                u'mpatt_eco_birds_goose_swan_density_winter_data': u'eco_birds_gooseswan_densitywinter_m',
                u'mpatt_eco_birds_gull_density_data': u'eco_birds_gull_density_m',
                u'mpatt_eco_birds_gull_density_winter_data': u'eco_birds_gull_densitywinter_m',
                u'mpatt_eco_birds_heron_density_winter_data': u'eco_birds_heron_densitywinter_m',
                u'mpatt_eco_birds_loon_and_grebe_density_data': u'eco_birds_loongrebe_density_d',
                u'mpatt_eco_birds_loon_grebe_density_winter_data': u'eco_birds_loongrebe_densitywinter_d',
                u'mpatt_eco_birds_marbledmurrelet_density_data': u'eco_birds_mamu_density_d',
                u'mpatt_eco_birds_marbledmurrelet_density_winter_data': u'eco_birds_mamu_densitywinter_d',
                u'mpatt_eco_birds_murre_largealcid_density_data': u'eco_birds_murrelgalcid_density_d',
                u'mpatt_eco_birds_murre_largealcid_density_winter_data': u'eco_birds_murrelgalcid_densitywinter_d',
                u'mpatt_eco_birds_pigeonguillemot_density_data': u'eco_birds_pigeonguillemot_density_m',
                u'mpatt_eco_birds_puffin_density_data': u'eco_birds_puffin_density_d',
                u'mpatt_eco_birds_seaduck_density_moulting_data': u'eco_birds_seaduck_densitymoulting_d',
                u'mpatt_eco_birds_seaduck_density_staging_data': u'eco_birds_seaduck_densitystaging_d',
                u'mpatt_eco_birds_seaduck_density_winter_data': u'eco_birds_seaduck_densitywinter_d',
                u'mpatt_eco_birds_shearwater_fulmar_density_data': u'eco_birds_shearwaterfulmar_density_d',
                u'mpatt_eco_birds_shorebird_mud_sand_density_data': u'eco_birds_shorebirdsand_density_d',
                u'mpatt_eco_birds_shorebird_rocky_density_data': u'eco_birds_shorebirdrock_density_d',
                u'mpatt_eco_birds_smallalcid_density_data': u'eco_birds_smallalcid_density_d',
                u'mpatt_eco_birds_smallalcid_density_winter_data': u'eco_birds_smallalcid_densitywinter_d',
                u'mpatt_eco_birds_stormpetrel_phalarope_density_data': u'eco_birds_petrelphalarope_density_d'}

# Start loop
for i in xrange(0,len(inFeat)):

    # --- Move feature into new gdb
    newFeat = inFeat[i]  # name of the feature class
    #get min Log value from previous file to use in the transformation
    arcpy.env.workspace = dataPrevTransf
    dfLegacy = pd.DataFrame(arcpy.da.FeatureClassToNumPyArray(matchFeat[inFeat[i]], ["reclassFin"]))
    dfLegacy['log'] = np.log(dfLegacy["reclassFin"])
    minLogLegacy = dfLegacy['log'].min()
    arcpy.env.workspace = ingdb # need this here because we change the workspace later and need to reset it at the start of each loop
    newFeatgdb = os.path.join(outgdb, newFeat) #name of the gdb/feature class

    # check for the feature in the gdb. If its already there, delete it and copy a new one in
    if arcpy.Exists(newFeatgdb):
        print("overwriting ", newFeatgdb)
        arcpy.Delete_management(newFeatgdb)
    else:
        print(newFeat, "not found -- creating")
    arcpy.FeatureClassToGeodatabase_conversion(newFeat,outgdb)

    # ---  Remove unsampled areas and clean up fields
    # Remove rows with <Null> values in reclassFinal (planning units with no data)
    arcpy.env.workspace = outgdb
    with arcpy.da.UpdateCursor(newFeat, "reclassFinal") as cursor:
        for row in cursor:
            if row[0] == None:
                cursor.deleteRow()

    # rename "reclassFinal" to densNorm by copying and deleting old field
    arcpy.AddField_management(newFeat, "densNorm", "FLOAT")
    arcpy.CalculateField_management(newFeat, "densNorm", "!reclassFinal!","PYTHON_9.3")

    # get list of all fields
    fields = arcpy.ListFields(newFeat)
    # for field in fields:
    #     print("Field: {0}".format(field.name))

    # define subset of fields to keep
    keepFields = ["PU_ID", "densNorm"]

    # make list of fields to drop
    fieldNameList = []
    for field in fields:
        if not field.required: # ignore 'required' fields to prevent errors
            if not field.name in keepFields:
                fieldNameList.append(field.name)

    # delete unwanted fields
    print("deleting fields")
    arcpy.DeleteField_management(newFeat, fieldNameList)

    # check it worked
    # fields = arcpy.ListFields(newFeat)
    # for field in fields:
    #     print("Field: {0}".format(field.name))

    # --- Calculate Quantiles
    # Bottom quantile needs to be dropped for the analysis version
    # The below script runs properly, but gives some differences to the version used ultimately in the 2019-20 analyses
    # Likely because of problems/differences in how quantiles were calculated, comparing float values, etc.
    # So, leave this part commented out because it works, but use minimum(log(x)) values from 2018-06_DataFromCarrie_Transformed

    # # Convert to numpy array and calculate quantiles
    # arr = arcpy.da.FeatureClassToNumPyArray(newFeat, 'densNorm')
    # arr = arr.astype('float32')
    #
    # # we've already removed planning units that have not been surveyed (where densNorm is <Null>)
    # # so the zeros in the array here represent areas that were surveyed but no birds were seen
    # # we want to calculate the quantiles of the actual observations
    # # so for the purpose of calculating quantiles, set 0s to NAN
    #
    # print("calculating quantiles")
    # arr[arr == 0] = np.nan
    #
    # # calculate 5 quantiles (each 20%)
    # p1 = np.nanpercentile(arr, 20)  # rank = 1
    # p2 = np.nanpercentile(arr, 40)  # rank = 2
    # p3 = np.nanpercentile(arr, 60)  # rank = 3
    # p4 = np.nanpercentile(arr, 80)  # rank = 4
    # p5 = np.nanpercentile(arr, 100)  # rank = 5
    #
    # #  convert to data frame and assign quantiles
    print('convert to dataframe')
    df = pd.DataFrame(arcpy.da.FeatureClassToNumPyArray(newFeat,["PU_ID","densNorm"]))

    # df['quantile'] = df['densNorm'].apply(lambda x: 0 if x == 0 else (1 if x <= p1
    #                                                                    else(2 if p1 <= x and x < p2
    #                                                                         else(3 if  p2 <= x and x < p3
    #                                                                              else(4 if p3 <= x and x < p4
    #                                                                                   else 5)))))
    #
    # # --- Transform data
    # # calculate the log of all values, and find the minimum value for quantiles 2-5 (exclude quantile 1)
    # print("transforming data")
    df['log'] = np.log(df["densNorm"]) # log transform. Output is a series with dtype float64

    #minLogTop4 = df[df['quantile']>1]['log'].min() # minimum of logs where quant >1. Is -6.183768 for gull winter dens, matching original data

    # apply the full transformation to the density value
    # the transformation is: log(densNorm[i]) + abs(min(log(densNorm) + 1
    df['densTrans'] = df['log'] + abs(minLogLegacy) + 1 # use legacy value - it's very close to what is calculated here, but not quite and we need the outputs to match

    # for analysis, only the top 4 quantiles will be used.
    # replace the transformed density for the bottom quantile with zero
    df['densTransTop'] = df['densTrans'] #copy the values to a new field

    # wrong way to do it:
    # df[df.quant==1]['densTransTop'] = 0 # warning ("A value is trying to be set on a copy of a slice from a DataFrame") because this is a get-set chained operation that doesnt change the actual data
    # df[df.quant==1]['densTransTop'] # can see the data was not actually changed.

    # right way:
    # df.loc[df['quantile'] == 1, 'densTransTop'] = np.nan  # using df.loc is a one-step set operation, which is required to conditionally replace in a pandas df
    # df.loc[df['quantile'] == 1, 'densTransTop']  # confirm the data was changed

    # using legacy value - set anything with log values below the legacy to nan. This would be the ones that were in the originally calculated quantile 1.
    df.loc[df['log'] < minLogLegacy, 'densTransTop'] = np.nan

    # --- Clean up
    # add 'value' field for CGA
    df['value'] = df['densTransTop']

    df = df.replace([np.inf, -np.inf], np.nan) # change infs to nans
    df.loc[np.isnan(df['value']),'value'] = 0 # for value field only, replace nan with 0

    # attach new fields back to FeatureClass
    dfnp = df.drop(['densNorm'], axis=1) # drop the column 'densNorm' as it's already in the Feature
    dfnp = dfnp.to_records(index=False)  # convert back to numpy array

    # merge dfnp to attribute table using PU_ID as key
    print("writing",newFeat)
    df.to_csv(outcsv+"_"+str(newFeat)+".csv", index=False)
    arcpy.da.ExtendTable(newFeatgdb, "PU_ID", dfnp, "PU_ID", append_only=True)

# confirmed that values match the 20190325 version.
# rename features to match
# clip feature to planning area
# project to bc albers
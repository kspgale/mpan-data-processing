#-------------------------------------------------------------------------------
# Title:        Complete formatting for bird density data for MPA Network analysis
#
# Authors:      Katie Gale
# Affiliation:  Fisheries and Oceans Canada (DFO)
# Group:        Marine Spatial Ecology and Analysis
# Location:     Institute of Ocean Sciences
# Contact:      katie.gale@dfo-mpo.gc.ca
# Project:      NSB MPA Network
#
# Version date: 8 July 2020
#
# Overview:
# - Start with bird density data after processing with transformBirdData.py (birds_reprocessed_2020-07-06.gdb)
# - Clip features to the MPATT planning area (MaPP_Marine_Area_EstuaryCorrected)
# - Project to BC Albers
# - Rename features to match the names they should have in the eco.gdb
#
# Requirements:
# * transformBirdData.py
# * birds_reprocessed_2020-07-06.gdb
# * clip feature: MaPP_Marine_Area_EstuaryCorrected.shp
# *
#-------------------------------------------------------------------------------

import os
import arcpy
from arcpy.sa import *
import math
import numpy as np
import regex
import pandas as pd

# Set environment settings
ingdb = r"D:\Documents\Data files\SpeciesData\Birds\2020-06-25_ReprocessBirdData\birds_reprocessed_2020-07-06.gdb"
arcpy.env.workspace = ingdb
inFeat = arcpy.ListFeatureClasses()

outgdb = r"D:\Documents\Data files\SpeciesData\Birds\2020-06-25_ReprocessBirdData\birds_clip_project_2020-07-06.gdb"

planningArea = r"D:\Documents\Data files\MPATT Data\DataLayers\Management Areas and Human Boundaries\mpatt_planning_areas\MaPP_Marine_Area_EstuaryCorrected\MaPP_Marine_Area_EstuaryCorrected.shp"

out_coordinate_system = arcpy.SpatialReference(3005) #BC Albers

#table for renaming features to match eco.gdb
matchFeat = {'mpatt_eco_birds_albatross_density_data': 'eco_birds_albatross_density_m',
                'mpatt_eco_birds_cormorant_density_data': 'eco_birds_cormorant_density_m',
                'mpatt_eco_birds_cormorant_density_winter_data': 'eco_birds_cormorant_densitywinter_m',
                'mpatt_eco_birds_goose_swan_density_data': 'eco_birds_gooseswan_density_m',
                'mpatt_eco_birds_goose_swan_density_winter_data': 'eco_birds_gooseswan_densitywinter_m',
                'mpatt_eco_birds_gull_density_data': 'eco_birds_gull_density_m',
                'mpatt_eco_birds_gull_density_winter_data': 'eco_birds_gull_densitywinter_m',
                'mpatt_eco_birds_heron_density_winter_data': 'eco_birds_heron_densitywinter_m',
                'mpatt_eco_birds_loon_and_grebe_density_data': 'eco_birds_loongrebe_density_d',
                'mpatt_eco_birds_loon_grebe_density_winter_data': 'eco_birds_loongrebe_densitywinter_d',
                'mpatt_eco_birds_marbledmurrelet_density_data': 'eco_birds_mamu_density_d',
                'mpatt_eco_birds_marbledmurrelet_density_winter_data': 'eco_birds_mamu_densitywinter_d',
                'mpatt_eco_birds_murre_largealcid_density_data': 'eco_birds_murrelgalcid_density_d',
                'mpatt_eco_birds_murre_largealcid_density_winter_data': 'eco_birds_murrelgalcid_densitywinter_d',
                'mpatt_eco_birds_pigeonguillemot_density_data': 'eco_birds_pigeonguillemot_density_m',
                'mpatt_eco_birds_puffin_density_data': 'eco_birds_puffin_density_d',
                'mpatt_eco_birds_seaduck_density_moulting_data': 'eco_birds_seaduck_densitymoulting_d',
                'mpatt_eco_birds_seaduck_density_staging_data': 'eco_birds_seaduck_densitystaging_d',
                'mpatt_eco_birds_seaduck_density_winter_data': 'eco_birds_seaduck_densitywinter_d',
                'mpatt_eco_birds_shearwater_fulmar_density_data': 'eco_birds_shearwaterfulmar_density_d',
                'mpatt_eco_birds_shorebird_mud_sand_density_data': 'eco_birds_shorebirdsand_density_d',
                'mpatt_eco_birds_shorebird_rocky_density_data': 'eco_birds_shorebirdrock_density_d',
                'mpatt_eco_birds_smallalcid_density_data': 'eco_birds_smallalcid_density_d',
                'mpatt_eco_birds_smallalcid_density_winter_data': 'eco_birds_smallalcid_densitywinter_d',
                'mpatt_eco_birds_stormpetrel_phalarope_density_data': 'eco_birds_petrelphalarope_density_d'}

# start loop
for i in xrange(0,len(inFeat)):
    arcpy.env.workspace = ingdb # reset workspace
    newFeat = inFeat[i] # name of the feature class
    newFeatgdb = os.path.join(outgdb, newFeat) #name of the gdb/feature class
    newName = matchFeat[newFeat] # name to rename output

    # check for the feature in the gdb. If its already there, delete it and copy a new one in
    if arcpy.Exists(newFeatgdb):
        print("overwriting ", newFeatgdb)
        arcpy.Delete_management(newFeatgdb)
    else:
        print(str(newFeat), "not found -- creating")
    arcpy.FeatureClassToGeodatabase_conversion(newFeat,outgdb)

    # clip feature with planning area
    print("... clipping")
    arcpy.env.workspace = outgdb
    arcpy.Clip_analysis(newFeat, planningArea, newFeat+"_clip")

    # project to BC Albers
    print("... projecting to BC Albers")
    arcpy.Project_management(newFeat+"_clip", newFeat+"_clip_proj", out_coordinate_system)

    # Clean up
    print("... deleting intermediate files")
    arcpy.Delete_management(newFeat)
    arcpy.Delete_management(newFeat+"_clip")

    print("... renaming output file")
    arcpy.Rename_management(newFeat+"_clip_proj", newName)

# end
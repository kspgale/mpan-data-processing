﻿# Marine Mammal effort-corrected density data for NSB MPAN Network
Written 19 June 2020

## Input data pre-processing
---------------
### DFO data
- Data was provided by DFO Marine Mammals in June 2017 in an access geodatabase (`Cetacean_Research_Program_SurveyData_20170329.mdb`). The .mdb contained features of: 
  - transect lines
  - polygons of the buffered transect lines (the sampling width or estimated visible range from the vessel). This was estimated for each vessel based on the elevation of the observation deck above the waterline, the prevailing weather conditions by vessel and weather conditions.
  - point locations and counts of species sightings.

### Initial data preparation/processing (Jessica Nephin did this):
- Calculate area sampled and effort over area, using a 25 km2 (5 km x 5 km) grid (`MarineMammalEffort.py`)
- Dissolve each buffer (individual buffers may have multiple line segments and overlapping polygons - dissolving minimizes overestimation/inconsistent estimation of survey effort)
- Overlay the grid with each buffer (1 buffer per transect)
- Sum the total area of all buffers in each grid cell. Effort = m^2
  - *Step done in oil spill version, but not MPAN or EBSA version: "minimum survey threshold was applied to the gridded data based on the number of surveys and the area surveyed within a grid cell to restrict the data set to grid cells that had been adequately sampled. Each grid cell required the presence of more than five surveys and the size of the total surveyed area to be at least the size of the grid cell to pass the threshold. Grid cells that did not meet the threshold were classified as ‘no data’ (NULL) and excluded from the dataset."*
- Overlay grid with sightings points (`MarineMammalObs.py`)
- Calculate density by species per grid cell (`DensityCalc.R`)
  - In each grid cell, sum marine mammal counts from all surveys for each species
  - Calculate density (counts/effort; sightings/km^2)
  
### NPPSD data
- Data was obtained from USGS (https://alaska.usgs.gov/products/data.php?dataid=1) 
- `NPSSD_Back_v2.mdb` was exported as `DATA_NPSSD.csv`
- no extra processing was done

### Raincoast data
- Data obtained from OBIS SeaMap (http://seamap.env.duke.edu/dataset/1485)
  - the data are estimated density for each species at points along the transects, which calculated using Multiple Covariate Distance Sampling (MCDS)
  - no extra processing was done
  
## Combining the 3 datasources and processing for MPAN (I did most of this)
---------------
### Rescaling/"Normalizing"
Scripts: `combineMammalData_2017.10.25.R`, `GroupByGrid.R`

Outputs: `combinedSurveys_species_rescale_01.shp`

- The three cetacean density datasets were normalized by dividing each value by the maximum value in each dataset. The resulting density index ranged from 0 to 1. 
- The normalized density estimates from the three cetacean datasets were overlaid onto a 5x5 km polygon grid and averaged together within each grid cell by species. 
- Non-detection (“absence”) values were kept from the Raincoast and DFO dataset, but not from the NPPSD dataset where cetacean observations were only opportunistically recorded. 

### Transforming
- The rescaled marine mammal density values ranged from 0-1, but were heavily skewed towards 0. This was constraining the prioritized planning units in Marxan, since there was such a difference between the highest and lowest values. 
- To normalize the distribution of values the rescaled density values were transformed: ln(n)+abs(min(ln(n)))+1. (i.e., for each density value, log the value, add the absolute value of the minimum logged density value for that species, plus 1.)
- Carrie did the transformation on the shapefiles (`SpeciesData\Mammals\2018-06_DataFromCarrie_Transformed`)
- I transformed the data in the PUVSPR (`MPATT Data\DataLayers\Data Processing\DataConversions_2018.06.06\Transform_PUVSPR_values`)

### Converting shape
- 20180606: Converted the 5 km grid cells to their associated 1 km grids. The 5 km grid was built to perfectly overlay the 1 km planning units. For each species, did a spatial join on the 5 km mammal data with the 1 km planning units (= `1kmfrom5km.shp`), then clipped those outputs with the 5 km data to remove 1 km planning units that were not represented in the 5 km layer (=`1kmfrom5kmClip`)
- Script `FormatMammalBirdData.R` added an extra field called `value` with the density value for each species (the CGA needed a `value` field). This script also removed all 0 cells, which **was probably not appropriate/not needed**. Marxan does not distinguish between zeros and NA's, so it doesn't matter for the analyses, but it does change how the data is visualized in the shapefile. In the shapefiles, the zeros are sampled areas with no observations, to compare with areas with no data shown at all. (folder \2020-06-18_MammalBirdAddValueRem0\)
- At some point the fields for rescaled and transformed values were removed, leaving only `value`.
- 20190325: All data was clipped to the MPAN planning area (`MaPP_Marine_Area_EstuaryCorrected`)
- 20190514: same as version 20190325, this is the version used in the eco.gdb for MPAN analyses 2019-2020

## 2020-06-19 Reprocessing for data archiving
---------------
For an archive version, I wanted:
 - Fields for the rescaled (pre-transformed) density values, the transformed values, and a field called `value`, which is a duplicate of the transformed value field. 
 - Zeros representing areas sampled
 - Outputs clipped to the planning area

Folder 2020-06-22_1kmFrom5kmClip_clipWith_mappMarineArea
 - Load `1kmfrom5kmClip` files (includes all original fields, and has sampled zeros)  
 - Keep fields `PU_ID`, rename fields `nDensity` to `densNorm` and `transDen` to `densTransf`, make field `value` from `transDen`
 - clip with `MaPP_Marine_Area_EstuaryCorrected` (`mpatt_planning_boundary` is shifted by about 50 m. The MaPPM Marine Area file seems to align with what was done previously.
 - save as "analysis version")
 - can also be used as "shareable version", though consider adding the 5-survey filter that was recommended.